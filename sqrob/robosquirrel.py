import copy

import numpy as np

from .entity import Entity
from .sprite import Sprite


enemy_sprites = {
    'robosquirrel': 'assets/images/robosquirrel.png',
    'cybersquirrel': 'assets/images/cybersquirrel.png',
    'kohnsquirrel': 'assets/images/kohnsquirrel.png'
}

enemy_parameters = {
    'robosquirrel': { 'follow': False },
    'cybersquirrel': { 'follow': True, 'health': 2 },
    'kohnsquirrel': { 'follow': True, 'damage': 0 }
}


class Enemy(Entity):
    def __init__(self, sprite, health=3, damage=0.1, follow=False):
        super().__init__()
        self.sprite = Sprite(enemy_sprites[sprite])
        self.randomize_direction()
        self.frame_update = 0
        self._follow = follow
        self.bullets = []
        self._health = health
        self.damage = damage

    @property
    def width(self):
        return self.sprite.clip_w

    @property
    def height(self):
        return self.sprite.clip_h

    def randomize_position(self, rect):
        """
        :param rect: Rectangle (x, y, w, h) in which to randomize
        """
        self.position = np.array([
            np.random.randint(rect[0], rect[2] - self.sprite.clip_w),
            np.random.randint(rect[1], rect[3] - self.sprite.clip_h)
        ])

    def randomize_direction(self):
        angle_rad = np.deg2rad(np.random.randint(0, 360))
        self.direction = np.array([
            np.cos(angle_rad) - np.sin(angle_rad),
            np.sin(angle_rad) + np.cos(angle_rad)
        ])

    def spawn(self, rect):
        self.position = np.array([
            np.random.choice([rect[0], rect[2] - self.sprite.clip_w]),
            np.random.choice([rect[1], rect[3] - self.sprite.clip_h])
        ])
        screen_center = np.array([
            rect[0] + rect[2] / 2,
            rect[1] + rect[3] / 2
        ])
        self.direction = screen_center - self.position
        self.direction /= np.linalg.norm(self.direction)

    def update(self, screen, player):
        if np.random.rand() < 0.01:
            self.randomize_direction()
        if self._follow:
            self.direction = player.position - self.position
            if np.linalg.norm(self.direction) != 0:
                self.direction = self.direction / np.linalg.norm(self.direction)
        # make robosquirrels bump at walls
        if self.position[0] < 0 and self.direction[0] < 0:
            self.direction[0] *= -1
        if self.position[1] < 0 and self.direction[1] < 0:
            self.direction[1] *= -1
        if self.position[0] + self.sprite.clip_w >= screen.get_width() and self.direction[0] > 0:
            self.direction[0] *= -1
        if self.position[1] + self.sprite.clip_h >= screen.get_height() and self.direction[1] > 0:
            self.direction[1] *= -1
        self.sprite.moving = True
        if self.frame_update == 0:
            self.sprite.next_x_frame()
        self.frame_update = (self.frame_update + 1) % 10
        if player.intersect((self.x + self.direction[0] + 5, self.y + self.direction[1] + 5, self.width - 10, self.height - 10)):
            if np.random.rand() < 0.05:
                self.randomize_direction()
            return
        self.position = self.direction + self.position

    def blit(self, screen):
        self.sprite.x = self.position[0]
        self.sprite.y = self.position[1]
        self.sprite.blit(screen, flip_x=self.direction[0] < 0)
        for bullet in self.bullets:
            bpos = np.copy(bullet.position)
            bullet.position += self.position
            bullet.blit(screen)
            bullet.position = bpos


def make_enemy(name):
    return Enemy(name, **enemy_parameters[name])