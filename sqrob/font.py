import copy

import pygame
import pygame.freetype

from .utils import load_image


class Font:
    def __init__(self, family, size):
        self.family = family
        self.size = size
        self.font = pygame.font.SysFont(family, size)
        self.texts = {}

    def get_text(self, string, color):
        colordict = self.texts.get(color, None)
        if colordict is None:
            self.texts[color] = {}
        text = self.texts[color].get(string, None)
        if text is None:
            self.texts[color][string] = self.font.render(string, True, color)
        return self.texts[color][string]

    def blit(self, screen, text, coordinates, color=(255, 255, 255)):
        text = self.get_text(text, color)
        screen.blit(text, coordinates)


class BitmapFont:
    def __init__(self, path, size=16):
        self.surface = load_image(path)
        self.surface = pygame.transform.scale(
            self.surface,
            (self.surface.get_width() * (size // 16), self.surface.get_height() * (size // 16))
        )
        self.colored_surfaces = {
            ((0, 0, 0), (128, 128, 128)): copy.copy(self.surface)
        }
        self.clip_w = size
        self.clip_h = size

    def set_color(self, border=(0, 0, 0), fill=(128, 128, 128)):
        idx = (border, fill)
        if idx not in self.colored_surfaces:
            self.colored_surfaces[idx] = copy.copy(self.surface)
            w, h = self.colored_surfaces[idx].get_size()
            for x in range(w):
                for y in range(h):
                    pixel = self.colored_surfaces[idx].get_at((x, y))
                    if pixel[3] == 0:
                        continue
                    if pixel[0] == 128 and pixel[1] == 128 and pixel[2] == 128:
                        self.colored_surfaces[idx].set_at((x, y), fill)
                    elif pixel[0] == 0 and pixel[1] == 0 and pixel[2] == 0:
                        self.colored_surfaces[idx].set_at((x, y), border)
        return self.colored_surfaces[idx]

    def write(self, screen, text, position, border=(0, 0, 0), color=(128, 128, 128)):
        tx = 0
        ty = 0
        surface = self.set_color(border, color)
        for char in text:
            if char == '\n':
                ty += 1
                tx = 0
                continue
            i = ord(char)
            clip_x = i % 8
            clip_y = i // 8
            screen.blit(
                surface,
                (position[0] + tx * self.clip_w, position[1] + ty * self.clip_h),
                (self.clip_w * clip_x, self.clip_h * clip_y, self.clip_w, self.clip_h)
            )
            tx += 1