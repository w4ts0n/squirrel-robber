import numpy as np

class Camera:
    def __init__(self, screen):
        self.position = np.array([0, 0])
        self.screen = screen

    def reset(self):
        self.position = np.array([0, 0])

    @property
    def dimensions(self):
        return self.screen.get_size()

    @property
    def x(self):
        return self.position[0]

    @property
    def y(self):
        return self.position[1]

    @x.setter
    def x(self, value):
        self.position[0] = value

    @y.setter
    def y(self, value):
        self.position[1] = value

    def __in__(self, data):
        pass