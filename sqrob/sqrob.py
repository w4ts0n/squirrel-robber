import time

import pygame

from pygame.locals import *

import click
import numpy as np

from .bullet import Bullet
from .camera import Camera
from .colors import GRASS_GREEN, WHITE
from .font import Font, BitmapFont
from .gameview import SquirrelGameView
from .numberfont import NumberFont
from .player import Player
from .robosquirrel import make_enemy
from .sprite import Sprite
from .ui import Application, View
from .utils import load_image, blit_rotated, load_sound, load_music
from .world import World


class MainMenuView(View):
    def __init__(self):
        self.title = load_image('assets/images/title.png')
        self.title = pygame.transform.scale2x(self.title)
        self.bitmapfont = BitmapFont('assets/images/font_16x16.png', 32)

        load_music('assets/sounds/ambient.mp3')
        pygame.mixer.music.play(-1)

        self.buttons = [
            '  START',
            'MUSIC OFF',
            'SOUND OFF',
            '  EXIT'
        ]
        self.selected = 0

    def run(self, app):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    self.selected = (self.selected - 1) % len(self.buttons)
                elif event.key == pygame.K_DOWN:
                    self.selected = (self.selected + 1) % len(self.buttons)
                elif event.key == pygame.K_SPACE:
                    if self.buttons[self.selected] == '  START':
                        app.push_view(SquirrelGameView())
                    elif self.buttons[self.selected] == 'MUSIC OFF':
                        pygame.mixer.music.fadeout(1500)
                        self.buttons[1] = 'MUSIC ON'
                    elif self.buttons[self.selected] == 'MUSIC ON':
                        pygame.mixer.music.play(-1)
                        self.buttons[1] = 'MUSIC OFF'
                    elif self.buttons[self.selected] == 'SOUND OFF':
                        app.sounds_enabled = False
                        self.buttons[2] = 'SOUND ON'
                    elif self.buttons[self.selected] == 'SOUND ON':
                        self.buttons[2] = 'SOUND OFF'
                        app.sounds_enabled = True
                    elif self.buttons[self.selected] == '  EXIT':
                        app.pop_view()
                elif event.key == pygame.K_ESCAPE:
                    app.pop_view()

        app.screen.fill((127, 127, 127))
        app.screen.blit(self.title, (0, 0))
        self.bitmapfont.write(app.screen, 'SQUIRREL\n   VS.\n ROBOTS', (240, 150), color=(255, 255, 255))
        for i, button in enumerate(self.buttons):
            color = (0, 127, 220) if i == self.selected else (128, 128, 128)
            self.bitmapfont.write(app.screen, button, (220, 300 + i * 50), color=color)


def run_app():
    app = Application('squirrel vs. robots')
    app.push_view(MainMenuView())
    app.run()


@click.command()
def main():
    run_app()


if __name__ == '__main__':
    main()
