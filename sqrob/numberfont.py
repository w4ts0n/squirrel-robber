import pygame

from .utils import load_image


class NumberFont:
    def __init__(self):
        self.surface = load_image('assets/images/numbers.png')
        self.surface = pygame.transform.scale2x(self.surface)
        self.clip_w = 16 * 2
        self.clip_h = 16 * 2

    def blit(self, screen, x, y, number):
        for i, c in enumerate(str(number)):
            frame = ord(c) - ord('0')
            screen.blit(
                self.surface,
                (x + i * self.clip_w, y),
                (self.clip_w * frame, 0, self.clip_w, self.clip_h)
            )