import numpy as np
import pygame

from .utils import random_hue_color


class Bullet:
    def __init__(self, position, direction, weapon):
        self.position = position
        self.direction = direction
        self.color = random_hue_color()
        self.alive = True
        self.size = 3
        self.weapon = weapon
        self.frame = np.random.randint(0, 3)

    def update(self):
        self.position[0] += self.direction[0] + np.random.randint(0, self.weapon.stability)
        self.position[1] += self.direction[1] + np.random.randint(0, self.weapon.stability)

    def blit(self, screen):
        if self.weapon.particles is None:
            pygame.draw.circle(screen, self.color, self.position, self.size)
        else:
            self.weapon.particles.position = self.position - np.array([8, 8])
            self.weapon.particles.x_frame = self.frame
            self.weapon.particles.blit(screen)

    def in_rect(self, rect):
        if self.position[0] < rect[0]:
            return False
        if self.position[1] < rect[1]:
            return False
        if self.position[0] >= rect[0] + rect[2]:
            return False
        if self.position[1] >= rect[1] + rect[3]:
            return False
        return True

    def hit(self, sprite):
        if not self.in_rect(sprite.bbox):
            return False
        pixel = sprite.get_at(
            self.position - sprite.position
        )
        return pixel[3] != 0