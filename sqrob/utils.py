import os

import colorsys
import numpy as np
import pygame


def random_hue_color(saturation=1.0, value=1.0):
    h = np.random.rand()
    rgb = colorsys.hsv_to_rgb(h, saturation, value)
    return (255 * rgb[0], 255 * rgb[1], 255 * rgb[2])


def load_image(path):
    packagedir = os.path.dirname(__file__)
    return pygame.image.load(os.path.join(packagedir, path))


def load_sound(path):
    packagedir = os.path.dirname(__file__)
    return pygame.mixer.Sound(os.path.join(packagedir, path))


def load_music(path):
    packagedir = os.path.dirname(__file__)
    pygame.mixer.music.load(os.path.join(packagedir, path))


def blit_rotated(screen, image, pos, pivot, angle):
    if angle >= 90 and angle <= 270:
        surface = pygame.transform.flip(image, flip_x=False, flip_y=True)
    else:
        surface = image
    image_rect = surface.get_rect(topleft=(pos[0] - pivot[0], pos[1] - pivot[1]))
    offset_center_to_pivot = pygame.math.Vector2(pos) - image_rect.center
    rotated_offset = offset_center_to_pivot.rotate(-angle)
    rotated_image_center = (pos[0] - rotated_offset.x, pos[1] - rotated_offset.y)
    rotated_image = pygame.transform.rotate(surface, angle)
    rotated_image_rect = rotated_image.get_rect(center=rotated_image_center)
    screen.blit(rotated_image, rotated_image_rect)