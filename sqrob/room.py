import numpy as np
import pygame

#import pytmx
#from pytmx.util_pygame import load_pygame
#import pyscroll

from .sprite import Sprite
from .utils import load_image


class Room:
    def __init__(self):
        self.grass_tiles = Sprite('assets/images/grass_tiles.png')
        self.grass_tiles.clip_w = 32
        self.grass_tiles.clip_h = 32
        self.floor = None

    def is_cleared(self):
        return False

    def blit(self, screen, camera):
        if self.floor is None:
            max_tiles = self.grass_tiles.surface.get_size()[0] // self.grass_tiles.clip_w
            self.tile_map = np.random.randint(0, max_tiles, size=(screen.get_width() // 32, screen.get_height() // 32))
            self.floor = pygame.Surface(screen.get_size())
            for ty in range(screen.get_height() // 32):
                for tx in range(screen.get_width() // 32):
                    self.grass_tiles.x_frame = self.tile_map[tx, ty]
                    self.grass_tiles.x = tx * 32
                    self.grass_tiles.y = ty * 32
                    self.grass_tiles.blit(self.floor)
        screen.blit(self.floor, (0, 0))
