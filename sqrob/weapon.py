import numpy as np
import pygame

from .sprite import Sprite
from .utils import load_image


class Weapon:
    def __init__(self, image_path, backlash, bullet_velocity, cooldown, damage, pivot, tip, stability, particles=None):
        self.image = load_image(image_path)
        self.image = pygame.transform.scale2x(self.image)
        self.backlash = backlash
        self.bullet_velocity = bullet_velocity
        self.cooldown = cooldown
        self.damage = damage
        self.pivot = pivot
        self.tip = tip
        self.stability = stability
        self.particles = None
        if particles is not None:
            self.particles = Sprite(particles, scale=1)


weapons = {
    'gun': Weapon(
        image_path='assets/images/gun.png',
        backlash=7, bullet_velocity=4, cooldown=20, damage=1,
        pivot=(3, 4), tip=20, stability=5
    ),
    'leafblower': Weapon(
        image_path='assets/images/leafblower.png',
        backlash=2, bullet_velocity=4, cooldown=1, damage=0.2,
        pivot=(3, 4), tip=40, stability=3,
        particles='assets/images/leaves.png'
    )
}
