import time

import pygame

from pygame.locals import *

import click
import numpy as np

from .bullet import Bullet
from .camera import Camera
from .colors import GRASS_GREEN, WHITE
from .font import Font, BitmapFont
from .numberfont import NumberFont
from .particles import ParticleSpawner
from .player import Player
from .robosquirrel import make_enemy
from .sprite import Sprite
from .ui import Application, View
from .utils import load_image, blit_rotated, load_sound, load_music
from .world import World


class SquirrelGameView(View):
    def __init__(self):
        self.bitmapfont = BitmapFont('assets/images/font_16x16.png', 32)
        self.number_font = NumberFont()

        self.world = World()

        self.player = Player()

        self.heart = Sprite('assets/images/heart.png')
        self.heart.clip_w = 16
        self.heart.clip_h = 16

        self.sound_explosion = load_sound(
            'assets/sounds/explosion.wav'
        )
        self.sound_leafblower = load_sound(
            'assets/sounds/leafblower.wav'
        )

        self.bullets = []
        self.robots = []
        self.particle_spawners = []

        self.robots_killed = 0
        self.weapon_cooldown = 0
        self.max_robots = 5

        self.gamestate = 'game'

    def run(self, app):
        if self.gamestate == 'game':
            # spawn new robots if necessary
            if len(self.robots) < self.max_robots:
                p_spawn = 0.01 * (1 / (len(self.robots) + 1))
                if np.random.rand() < p_spawn:
                    if (self.robots_killed + len(self.robots)) % 16 == 0 and self.robots_killed > 0:
                        robot = make_enemy('kohnsquirrel')
                    elif np.random.rand() < 0.2:
                        robot = make_enemy('cybersquirrel')
                    else:
                        robot = make_enemy('robosquirrel')
                    robot.spawn((0, 0, app.screen.get_width(), app.screen.get_height()))
                    self.robots.append(robot)
            # update & blit robots
            for robot in self.robots:
                robot.update(app.screen, self.player)
                if robot.alive and self.player.intersect(robot.bbox) and self.player.frame_update == 0:
                    self.player.health -= robot.damage

            # remove dead or out-of-bounds robots
            self.robots = [
                r for r in self.robots
                if r.alive and r.sprite.in_rect((-64, -64, app.screen.get_width() + 64, app.screen.get_height() + 64))
            ]

            if self.weapon_cooldown < self.player.weapon.cooldown:
                self.weapon_cooldown += 1

            def shoot():
                if self.weapon_cooldown == self.player.weapon.cooldown:
                    if self.player.frame_update == 0:
                        if app.sounds_enabled:
                            self.sound_leafblower.play()
                    angle_rad = np.deg2rad(360 - self.player.weapon_angle - 45)
                    direction = np.array([
                        np.cos(angle_rad) - np.sin(angle_rad),
                        np.sin(angle_rad) + np.cos(angle_rad)
                    ])
                    # shake weapon
                    self.player.weapon_angle += self.player.weapon.stability * (np.random.rand() - 0.5)
                    bullet = Bullet(
                        self.player.weapon_position + self.player.weapon.tip * direction,
                        self.player.weapon.bullet_velocity * direction,
                        self.player.weapon
                    )
                    self.bullets.append(bullet)
                    self.weapon_cooldown = 0
                    # backlash
                    self.player.move(-self.player.weapon.backlash * direction)

            def left():
                self.player.x -= self.player.velocity
                for robot in self.robots:
                    if not robot.alive:
                        continue
                    if self.player.intersect(robot.bbox):
                        self.player.health -= robot.damage
                        self.player.x += self.player.velocity
                        return
                self.player.x += self.player.velocity
                self.player.left(app.screen)

            def right():
                self.player.x += self.player.velocity
                for robot in self.robots:
                    if not robot.alive:
                        continue
                    if self.player.intersect(robot.bbox):
                        self.player.health -= robot.damage
                        self.player.x -= self.player.velocity
                        return
                self.player.x -= self.player.velocity
                self.player.right(app.screen)

            def up():
                self.player.y -= self.player.velocity
                for robot in self.robots:
                    if not robot.alive:
                        continue
                    if self.player.intersect(robot.bbox):
                        self.player.health -= robot.damage
                        self.player.y += self.player.velocity
                        return
                self.player.y += self.player.velocity
                self.player.up(app.screen)

            def down():
                self.player.y += self.player.velocity
                for robot in self.robots:
                    if not robot.alive:
                        continue
                    if self.player.intersect(robot.bbox):
                        self.player.health -= robot.damage
                        self.player.y -= self.player.velocity
                        return
                self.player.y -= self.player.velocity
                self.player.down(app.screen)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    app.pop_view()
            if len(app.joysticks) > 0:
                joy = app.joysticks[0]
                if abs(joy.get_axis(0)) > 0.01:
                    if joy.get_axis(0) < -0.1:
                        left()
                    elif joy.get_axis(0) > 0.1:
                        right()
                if abs(joy.get_axis(1)) > 0.01:
                    if joy.get_axis(1) < -0.1:
                        up()
                    elif joy.get_axis(1) > 0.1:
                        down()
                if joy.get_button(0):
                    shoot()
                if joy.get_button(1):
                    self.player.weapon_angle += 2 * self.player.direction[0]
                if joy.get_button(3):
                    self.player.weapon_angle -= 2 * self.player.direction[0]

            keys = pygame.key.get_pressed()
            if keys[pygame.K_ESCAPE]:
                app.pop_view()
            if keys[pygame.K_SPACE]:
                shoot()
            if keys[pygame.K_a]:
                self.player.weapon_angle += 2 * self.player.direction[0]
            elif keys[pygame.K_d]:
                self.player.weapon_angle -= 2 * self.player.direction[0]
            if keys[pygame.K_LEFT]:
                left()
            elif keys[pygame.K_RIGHT]:
                right()
            if keys[pygame.K_UP]:
                up()
            elif keys[pygame.K_DOWN]:
                down()

            for bullet in self.bullets:
                bullet.update()
                for robot in self.robots:
                    if bullet.hit(robot.sprite):
                        robot.health -= self.player.weapon.damage
                        bullet.position -= robot.position
                        bullet.size = np.random.randint(3, 6)
                        robot.bullets.append(bullet)
                        bullet.alive = False
                        if not robot.alive:
                            if app.sounds_enabled:
                                self.sound_explosion.play()
                            self.robots_killed += 1
                            particle_spawner = ParticleSpawner(
                                robot.position + np.array([32, 32]),
                                life=75
                            )
                            self.particle_spawners.append(particle_spawner)
            # remove out-of-view bullets
            self.bullets = [
                b for b in self.bullets
                if b.in_rect((0, 0, app.screen.get_width(), app.screen.get_height())) and b.alive
            ]

            for particle_spawner in self.particle_spawners:
                particle_spawner.update()
            # remove decayed particle spawners
            self.particle_spawners = [
                p for p in self.particle_spawners
                if p.life != 0 or len(p.particles) > 0
            ]

        self.world.current_room.blit(app.screen, app.camera)

        for robot in self.robots:
            robot.blit(app.screen)
        self.player.blit(app.screen)
        for bullet in self.bullets:
            bullet.blit(app.screen)
        for particle_spawner in self.particle_spawners:
            particle_spawner.blit(app.screen)

        self.number_font.blit(app.screen, 8, 8, self.robots_killed)

        # display health points
        h = self.player.health
        for i in range(5):
            if h > 1:
                self.heart.x_frame = 6
            else:
                if h > 0:
                    self.heart.x_frame = int(h * 6)
                else:
                    self.heart.x_frame = 0
            h -= 1
            self.heart.x = app.screen.get_width() - (i + 1) * 20
            self.heart.y = 8
            self.heart.blit(app.screen)

        if not self.player.alive:
            self.gamestate = 'lost'

        if self.gamestate == 'lost':
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    app.pop_view()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        app.pop_view()
            pygame.draw.rect(app.screen, (128, 128, 128, 50), (180, 180, 400, 200))
            self.bitmapfont.write(app.screen, 'GAME OVER!', (200, 200), color=(255, 0, 0))
            self.bitmapfont.write(app.screen, 'Press SPACE', (200, 260), color=(255, 255, 255))
            self.bitmapfont.write(app.screen, 'to continue.', (200, 300), color=(255, 255, 255))
