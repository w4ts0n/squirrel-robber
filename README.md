# squirrel-robber

Work-in-progress game about a squirrel that has to save the world from evil robo-squirrels.

![](static/teaser.gif)

## Installation


### Via setup.py

Install the project via:

```bash
sudo python3 ./setup.py install
```

and run

```bash
squirrel-robber
```

to start the game.