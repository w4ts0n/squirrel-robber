#!/usr/bin/env python3

from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')


setup(
    name='sqrob',
    version='0.1.0',
    description='',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='eichkat3r',
    author_email='mail@katerbase.de',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3 :: Only',
    ],
    keywords='squirrel, game',
    packages=['sqrob'],
    python_requires='>=3.7, <4',
    install_requires=['pygame', 'click', 'numpy'],
    package_data={
        'sqrob': [
            'assets/*', 
            'assets/images/*',
            'assets/sounds/*'
        ]
    },
    data_files = [
        ('share/applications', ['squirrel-robber.desktop']),
        ('share/icons', ['squirrel-robber.png'])
    ],
    entry_points="""
        [console_scripts]
        squirrel-robber = sqrob:main
    """,
)
